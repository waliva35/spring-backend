package com.wlinares.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wlinares.model.Especialidad;

public interface IEspecialidadDao extends JpaRepository<Especialidad, Integer> {

}
