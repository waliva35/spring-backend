package com.wlinares.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wlinares.model.SignoVital;

public interface ISignoVitalDAO extends JpaRepository<SignoVital, Integer>
{
	@Query("from SignoVital sv where sv.paciente.idPaciente=:idPaciente")
	List<SignoVital> buscarSignoVitalPorPaciente(@Param("idPaciente")Integer idPaciente);
}
