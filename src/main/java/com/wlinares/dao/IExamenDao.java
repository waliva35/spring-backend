package com.wlinares.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wlinares.model.Examen;

public interface IExamenDao extends JpaRepository<Examen, Integer> {

}
