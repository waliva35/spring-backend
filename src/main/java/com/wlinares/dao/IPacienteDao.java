package com.wlinares.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wlinares.model.Paciente;

public interface IPacienteDao extends JpaRepository<Paciente, Integer>
{

}
