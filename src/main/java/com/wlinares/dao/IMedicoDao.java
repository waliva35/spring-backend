package com.wlinares.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wlinares.model.Medico;

public interface IMedicoDao extends JpaRepository<Medico, Integer>{

}
