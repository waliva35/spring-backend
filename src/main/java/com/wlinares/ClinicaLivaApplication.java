package com.wlinares;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicaLivaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClinicaLivaApplication.class, args);
	}

}
