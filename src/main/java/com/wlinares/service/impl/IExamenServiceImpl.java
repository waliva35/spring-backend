package com.wlinares.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import com.wlinares.dao.IExamenDao;
import com.wlinares.model.Examen;
import com.wlinares.service.IExamenService;

@Service
public class IExamenServiceImpl implements IExamenService {

	@Autowired
	private IExamenDao dao;
	
	@Override
	public Examen registrar(Examen t) {
		return dao.save(t);
	}

	@Override
	public Examen modificar(Examen t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Examen listarId(int id) {
		Optional<Examen> op = dao.findById(id);
		return op.get();
	}

	@Override
	public List<Examen> listar() {
		return dao.findAll();
	}

}
