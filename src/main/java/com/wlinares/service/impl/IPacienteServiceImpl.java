package com.wlinares.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wlinares.dao.IPacienteDao;
import com.wlinares.model.Paciente;
import com.wlinares.service.IPacienteService;

@Service
public class IPacienteServiceImpl implements IPacienteService {

	@Autowired
	private IPacienteDao dao;
	
	@Override
	public Paciente registrar(Paciente t) {
		return dao.save(t);
	}

	@Override
	public Paciente modificar(Paciente t) {
		return dao.save(t);   
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public Paciente listarId(int id) {
		Optional<Paciente> op = dao.findById(id);
		return op.get();
	}

	@Override
	public List<Paciente> listar() {
		return dao.findAll();
	}

}
