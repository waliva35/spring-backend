package com.wlinares.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.wlinares.dao.ISignoVitalDAO;
import com.wlinares.model.SignoVital;
import com.wlinares.service.ISignoVitalService;

@Service
public class ISignoVitalServiceImp implements ISignoVitalService {

	
	@Autowired
	private ISignoVitalDAO dao;
	
	@Override
	public SignoVital registrar(SignoVital t) {
		return dao.save(t);
	}

	@Override
	public SignoVital modificar(SignoVital t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
		
	}

	@Override
	public SignoVital listarId(int id) {
		Optional<SignoVital> op = dao.findById(id);
		return op.get();
	}

	@Override
	public List<SignoVital> listar() {
		return dao.findAll();
	}

	@Override
	public List<SignoVital> buscarSignoVitalPorPaciente(Integer idPaciente) {
		return dao.buscarSignoVitalPorPaciente(idPaciente);
	}

}
