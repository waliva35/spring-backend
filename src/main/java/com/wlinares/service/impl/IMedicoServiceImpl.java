package com.wlinares.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wlinares.dao.IMedicoDao;
import com.wlinares.model.Medico;
import com.wlinares.service.IMedicoService;

@Service
public class IMedicoServiceImpl implements IMedicoService {

	@Autowired
	private IMedicoDao dao;
	
	@Override
	public Medico registrar(Medico t) {
		return dao.save(t);
	}

	@Override
	public Medico modificar(Medico t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Medico listarId(int id) {
		Optional<Medico> op = dao.findById(id);
		return op.get();
	}

	@Override
	public List<Medico> listar() {
		return dao.findAll();
	}

}
