package com.wlinares.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wlinares.dao.IEspecialidadDao;
import com.wlinares.model.Especialidad;
import com.wlinares.service.IEspecialidadService;

@Service
public class IEspecialidadServiceImpl implements IEspecialidadService {

	@Autowired
	private IEspecialidadDao dao;
	
	@Override
	public Especialidad registrar(Especialidad t) {
		return dao.save(t);
	}

	@Override
	public Especialidad modificar(Especialidad t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Especialidad listarId(int id) {
		Optional<Especialidad> op = dao.findById(id);
		return op.get();
	}

	@Override
	public List<Especialidad> listar() {
		return dao.findAll();
	}
	
	

}
