package com.wlinares.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wlinares.dao.IConsultaDao;
import com.wlinares.dao.IConsultaExamenDAO;
import com.wlinares.dao.IExamenDao;
import com.wlinares.dto.ConsultaListaExamenDTO;
import com.wlinares.dto.FiltroConsultaDTO;
import com.wlinares.model.Consulta;
import com.wlinares.service.IConsultaService;

@Service
public class IConsultaServiceImpl implements IConsultaService{

	@Autowired
	private IConsultaDao dao;
	
	@Autowired
	private IConsultaExamenDAO edao;
	
	/*
	@Override
	public Consulta registrar(Consulta consulta) {
		consulta.getDetalleConsulta().forEach(d -> {
			d.setConsulta(consulta);
		});
		return dao.save(consulta);
	}*/
	@Transactional
	@Override
	public Consulta registrarTransaccional(ConsultaListaExamenDTO consultaDTO) {
		
		consultaDTO.getConsulta().getDetalleConsulta().forEach(d->{
			d.setConsulta(consultaDTO.getConsulta());
		});
		
		dao.save(consultaDTO.getConsulta());
		
		consultaDTO.getLstExamen().forEach(e ->
			edao.registrar(consultaDTO.getConsulta().getIdConsulta(), e.getIdExamen()));
		
		return consultaDTO.getConsulta();
		
	}

	@Override
	public Consulta modificar(Consulta t) {
		return dao.save(t);
	}

	@Override
	public void eliminar(int id) {
		dao.deleteById(id);
	}

	@Override
	public Consulta listarId(int id) {
		Optional<Consulta> op = dao.findById(id);
		return op.get();
	}

	@Override
	public List<Consulta> listar() {
		return dao.findAll();
	}

	@Override
	public Consulta registrar(Consulta t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Consulta> buscar(FiltroConsultaDTO filtro) {
		return dao.buscar(filtro.getDni(), filtro.getNombreCompleto());
	}

	@Override
	public List<Consulta> buscarFecha(FiltroConsultaDTO filtro) {
		LocalDateTime fechaSgte = filtro.getFechaConsulta().plusDays(1);
		return dao.buscarFecha(filtro.getFechaConsulta(), fechaSgte);
	}

}
