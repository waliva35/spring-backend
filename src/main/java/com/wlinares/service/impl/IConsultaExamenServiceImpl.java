package com.wlinares.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wlinares.dao.IConsultaExamenDAO;
import com.wlinares.model.ConsultaExamen;
import com.wlinares.service.IConsultaExamenService;

@Service
public class IConsultaExamenServiceImpl implements  IConsultaExamenService{

	@Autowired
	private IConsultaExamenDAO dao;
	
	@Override
	public List<ConsultaExamen> listarExamenPorConsulta(Integer idConsulta) {
		return dao.listarExamenPorConsulta(idConsulta);
	}
	

}
