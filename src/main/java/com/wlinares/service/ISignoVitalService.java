package com.wlinares.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.wlinares.model.SignoVital;

public interface ISignoVitalService extends ICRUD<SignoVital> {
	
	List<SignoVital> buscarSignoVitalPorPaciente(Integer idPaciente);

}
