package com.wlinares.service;

import java.util.List;

import com.wlinares.model.ConsultaExamen;

public interface IConsultaExamenService {

	List<ConsultaExamen>listarExamenPorConsulta(Integer idConsulta);
}
