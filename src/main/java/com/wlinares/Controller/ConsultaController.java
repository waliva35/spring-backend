package com.wlinares.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wlinares.dto.ConsultaListaExamenDTO;
import com.wlinares.dto.FiltroConsultaDTO;
import com.wlinares.model.Consulta;
import com.wlinares.service.IConsultaService;

@RestController
@RequestMapping("/consultas")
public class ConsultaController {

	@Autowired
	private IConsultaService service;
	
	@GetMapping
	public List<Consulta> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Consulta listaPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	/*@PostMapping(consumes = "application/json",produces = "application/json")
	public Consulta registrar(@RequestBody Consulta consulta) {
		return service.registrar(consulta);
	}*/
	
	@PostMapping(consumes = "application/json",produces = "application/json")
	public Consulta registrar(@RequestBody ConsultaListaExamenDTO consultaDTO) {
		return service.registrarTransaccional(consultaDTO);
	}
	
	@PutMapping(consumes = "application/json",produces = "application/json")
	public Consulta actualizar(@RequestBody Consulta consulta) {
		return service.modificar(consulta);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
	
	@PostMapping(value="/buscar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Consulta>>buscar(@RequestBody FiltroConsultaDTO filtro){
		List<Consulta> consultas = new ArrayList<>();
		if(filtro != null) {
			if(filtro.getFechaConsulta() != null) {
				consultas = service.buscarFecha(filtro);
			}else {
				consultas = service.buscar(filtro);
			}
		}
		return new ResponseEntity<List<Consulta>>(consultas,HttpStatus.OK);
		
	}
	
}
