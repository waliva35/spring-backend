package com.wlinares.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wlinares.model.Especialidad;
import com.wlinares.service.IEspecialidadService;

@RestController
@RequestMapping("/especialidades")
public class EspecialidadController {

	@Autowired
	private IEspecialidadService service;
	
	@GetMapping
	public List<Especialidad> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Especialidad listaPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(consumes = "application/json",produces = "application/json")
	public Especialidad registrar(@RequestBody Especialidad especialidad) {
		return service.registrar(especialidad);
	}
	
	@PutMapping(consumes = "application/json",produces = "application/json")
	public Especialidad actualizar(@RequestBody Especialidad especialidad) {
		return service.modificar(especialidad);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
	
}
