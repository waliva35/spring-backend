package com.wlinares.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.wlinares.exception.ModeloNotFoundException;
import com.wlinares.model.Paciente;
import com.wlinares.model.SignoVital;
import com.wlinares.service.ISignoVitalService;

@RestController
@RequestMapping("/signovital")
public class SignoVitalController {
	
	@Autowired
	private ISignoVitalService service;
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<SignoVital>> listar(){		
		List<SignoVital> signo = new ArrayList<>();
		signo = service.listar();
		return new ResponseEntity<List<SignoVital>>(signo,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<SignoVital> listaPorId(@PathVariable("id") Integer id) {
		SignoVital signo = new SignoVital();
		signo = service.listarId(id);
		return new ResponseEntity<SignoVital>(signo,HttpStatus.OK);
	}
	
	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> registrar(@RequestBody SignoVital signo){
		SignoVital sig = new SignoVital();
		sig = service.registrar(signo);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(sig.getIdSigno()).toUri();
		return ResponseEntity.created(location).build();
	}
	
	@PutMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Object> modificar(@RequestBody SignoVital signo) {
		service.modificar(signo);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		SignoVital sig = service.listarId(id);
		if (sig == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO:" + id);
		}else {
			service.eliminar(id);
		}
	}
	
	@GetMapping(value="/buscar/{idPaciente}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SignoVital>> listarSignoPorPaciente(@PathVariable("idPaciente") Integer idPaciente) {
		List<SignoVital> listSigno = new ArrayList<>();
		listSigno = service.buscarSignoVitalPorPaciente(idPaciente);
		return new ResponseEntity<List<SignoVital>>(listSigno,HttpStatus.OK);
	}
	
	
	
}
