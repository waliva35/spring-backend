package com.wlinares.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wlinares.model.Medico;
import com.wlinares.service.IMedicoService;

@RestController
@RequestMapping("/medicos")
public class MedicoController {

	@Autowired
	private IMedicoService service;
	
	@GetMapping
	public List<Medico> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Medico listaPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	@PostMapping(consumes = "application/json",produces = "application/json")
	public Medico registrar(@RequestBody Medico medico) {
		return service.registrar(medico);
	}
	
	@PutMapping(consumes = "application/json",produces = "application/json")
	public Medico actualizar(@RequestBody Medico medico) {
		return service.modificar(medico);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		service.eliminar(id);
	}
	
	
}
