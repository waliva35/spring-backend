package com.wlinares.Controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.wlinares.exception.ModeloNotFoundException;
import com.wlinares.model.Paciente;
import com.wlinares.service.IPacienteService;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {
	
	@Autowired
	private IPacienteService service;
	
	/*
	@GetMapping(produces = "application/json")
	public List<Paciente> listar(){
		return service.listar();
	}*/
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Paciente>> listar(){
		List<Paciente> pacientes = new ArrayList<>();
		pacientes = service.listar();
		return new ResponseEntity<List<Paciente>>(pacientes,HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public Paciente listaPorId(@PathVariable("id") Integer id) {
		return service.listarId(id);
	}
	
	/*
	@PostMapping(consumes = "application/json",produces = "application/json")
	public Paciente registrar(@RequestBody Paciente paciente) {
		return service.registrar(paciente);
	}*/
	
	@PostMapping(consumes = "application/json",produces = "application/json")
	public ResponseEntity<Object> registrar(@RequestBody Paciente paciente) {
		Paciente pac = new Paciente();
		pac = service.registrar(paciente);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pac.getIdPaciente()).toUri();		
		return ResponseEntity.created(location).build();
	}
	
	/*
	@PutMapping(consumes = "application/json",produces = "application/json")
	public Paciente actualizar(@RequestBody Paciente paciente) {
		return service.modificar(paciente);
	}*/
	
	@PutMapping(consumes = "application/json",produces = "application/json")
	public ResponseEntity<Object> actualizar(@RequestBody Paciente paciente) {
		service.modificar(paciente);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		Paciente pac = service.listarId(id);
		if(pac == null) {
			throw new ModeloNotFoundException("ID NO ENCOTRADO:" + id);
		}else {
			service.eliminar(id);
		}
	}
	
	
	

}
